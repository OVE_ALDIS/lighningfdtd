# -*- coding: utf-8 -*-
"""
Created on Wed Mar 22 17:32:43 2023

@author: kohlmann
"""

import matplotlib.pyplot as plt
from scipy import signal, special
from scipy.fftpack import fft,ifft
#from scipy import special
import numpy as np

c0 = 299792458
mu0 = 4*np.pi*1e-7
eps0 = 1/(c0**2 * mu0)

def compute_lossy_ground_fields(w, dist_m, depth_m, sigma_1, eps_r1):
    pass

def compute_cooray_rubinstein(w, sigma, epsr):
    """
    Computes the Coory's formulation of stratified ground
    as given in Shoory 2010
    
    Input: w ... angular frequency omega
    """
    if w == 0:
        return 0+0*1j
    try:
        F_str = np.sqrt(mu0)/np.sqrt(epsr*eps0 + sigma/(1j*w))

        return F_str
    except:
        print("Problem occurred, return 0")
        raise
        return 0+0*1j
    
def compute_wave_tilt_formula(w, sigma, epsr):
    """
    Computes the Coory's formulation of stratified ground
    as given in Shoory 2010
    
    Input: w ... angular frequency omega
    """
    if w == 0:
        return 0+0*1j
    try:
#        F_str = np.sqrt(mu0)/np.sqrt(epsr*eps0 - sigma/(1j*w))
        F_str = 1/np.sqrt(epsr + sigma/(1j*w*eps0))

        return F_str
    except:
        print("Problem occurred, return 0")
        raise
        return 0+0*1j
    

def compute_underground_response(t, sigma, epsr, depth_m):
    t_z = -depth_m*np.sqrt(mu0*eps0*epsr)
    print("t_z", t_z)
    a = sigma/(eps0*epsr)
    print("a", a)
    y_t = np.exp(-a*t/2)*a*t_z/(2*np.sqrt(t**2-t_z**2))*special.iv(0, a*np.sqrt(t**2 - t_z**2))
    print(y_t[0:20])
    print("y(0) term:", np.exp(-a*t_z/2))
    #y_t[0] = y_t[0] + np.exp(-a*t_z/2)
    print(y_t[0:20])
    if t == 0:
        return y_t + np.exp
    else:
        return y_t

def compute_underground_response_w(w, sigma, epsr, depth_m):
    gamma = np.sqrt(1j*w*mu0*sigma - w**2*mu0*eps0*epsr)
    
    return eps0*np.exp(-gamma*depth_m)/(sigma+1j*w*eps0*epsr)

def compute_rubinstein_1996_eq3(w, sigma, epsr, depth_m):
    if w == 0:
        return 0+0j
    else:
        return np.exp(1j*w*np.sqrt(mu0*(epsr*eps0-1j*sigma/w))*depth_m)
