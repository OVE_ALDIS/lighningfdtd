#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 12:08:00 2019

@author: hannes
"""

import csv
import h5py
import numpy as np
import scipy
from scipy import signal, special
from scipy.fftpack import fft,ifft
import math as m
import sys, os
import matplotlib.pyplot as plt

from utils.utils import *
from utils.Wait_stratified_ground import *
from utils.lossy_ground import *


def plot_reference_fields(distance_km, 
                          h, 
                          sigma1, 
                          sigma2, 
                          eps_r1, 
                          eps_r2, 
                          plot_PEC = True,
                          plot_filtered = False,
                          typ = 'stratified',
                          underground_m = 0,
                          filtered_linestyle = "-",
                          time_snippet_us=20,
                          plot_field = "efield",
                          field_color="blue",
                          axis_color = "black",
                          linestyle = "-",
                          linewidth = "1.5",
                          label = "",
                          file_path="",
                          ax = None,):

    if ax==None:
        raise Exception("No axis for plotting received in plot_reference_fields. Please pass plt axis to parrameter 'ax'")
    
    distance_m = distance_km * 1000
    
    time_offset_us = distance_m/c0/1e-6
    oversampling = 1
    fields = {}
    
    
    with open(file_path) as file:
        data = csv.reader(file,delimiter="\t")
        csv_overlay = [r for r in data]
        #starttime = float([r[1].split(" ")[1] for r in csv_overlay[0:10] if "Starttime" in r[0]][0]) # hmmm nomnomnom
        dt = float([r[1].split(" ")[1] for r in csv_overlay[0:10] if "Unit X-Axis" in r[0]][0]) # hmmm nomnomnom
        print("Δt =", dt)
        overlay = {}
        overlay["length"] = int(csv_overlay[3][1])
        print("Length: {}".format(overlay["length"]))
        overlay["starttime"] = float(csv_overlay[4][1].strip(" s")) # time in microsec
        print("Starttime: {}".format(overlay["starttime"]))
        overlay["duration"] = overlay["length"]*dt
        overlay["time"] = np.array([i*(overlay["duration"])/overlay["length"] for i in range(overlay["length"])])
        first_data_entry = len(csv_overlay)-overlay["length"]
        overlay["efield"] = [float(row[0]) for idx, row in enumerate(csv_overlay) if idx>=first_data_entry]
        overlay["hfield"] =  [-float(row[5])/mu0 for idx, row in enumerate(csv_overlay) if idx>=first_data_entry]
        dt_oversample = dt/oversampling
        t_oversampled = np.r_[0:overlay["duration"]-oversampling*dt_oversample:dt_oversample]
        
        fields["efield"] = scipy.interpolate.interp1d(overlay["time"], overlay["efield"], "linear")
        fields["hfield"] = scipy.interpolate.interp1d(overlay["time"], overlay["hfield"], "linear")

        time_idx = np.where(overlay['time'] <= time_snippet_us*1e-6)
        overlay["efield"] = fields["efield"](t_oversampled)[time_idx]
        overlay["hfield"] = fields["hfield"](t_oversampled)[time_idx]
        overlay["time"] = t_oversampled
        time = overlay['time'][time_idx]
        dt = t_oversampled[1]-t_oversampled[0]
        
        
    d_freq = 1/dt/len(overlay['time'])
    freq = np.r_[0:1/2*1/dt:d_freq]
    w = 2*np.pi*freq
    
    
    #ax.set_title(r"E$_z$ and H$_y$ over PEC (d = {} km)".format(distance_km))
    if plot_PEC:
        ax.plot(time*1e6 + time_offset_us,  overlay[plot_field], color=field_color, linestyle = linestyle, linewidth=linewidth, label=label)
        ax.set_xlabel("Time (μs)")
        if plot_field == "efield":
            ax.set_ylabel("Ez (V/m)", color = axis_color)
        elif plot_field == "hfield":
            ax.set_ylabel("Hy (A/m)", color = field_color)
        ax.set_xlim(time_offset_us-0.3, time_snippet_us+time_offset_us)
        ax.set_ylim(0, 1.1*np.max(overlay[plot_field]))
        ax.tick_params(axis='y', labelcolor=field_color)
    
    if typ == 'stratified':
        # Use w (!!) not freq -.-
        F_ground =  np.array(list(map(lambda omega: compute_F_str_Wait(omega,distance_m, h, sigma1, sigma2, eps_r1, eps_r2), w)))
        F_ground_upper = np.conj(F_ground[::-1])
        #freq_minus = -freq[::-1]
        F_ground = np.hstack([F_ground[0:-1], F_ground_upper])
        freq_full = np.hstack([freq[0:-1], freq[-1]+freq])

        #f_ground = np.roll(ifft(F_ground), 1) 
        f_ground = ifft(F_ground)
        t_arr = np.array([i*dt for i in range(len(f_ground))])*1e6

        fields["efield_filtered"] = np.convolve(overlay["efield"], f_ground, mode="full")
        fields["hfield_filtered"] = np.convolve(overlay["hfield"], f_ground, mode="full")
        
        time_vector = (np.array(overlay["time"]))*1e6
        
        if plot_filtered:
            field = "efield_filtered" if plot_field == "efield" else "hfield_filtered"
            ax.plot(time_vector + time_offset_us, fields[field][0:len(time_vector)], color=field_color, linestyle=filtered_linestyle, linewidth=linewidth, label= label)
            ax.set_ylim(0, 1.1*np.max(fields[field][0:len(time_vector)]))
    
    elif typ == 'flat_lossy':
        print("flat lossy ****************************")
        
        ######################## TEST ################################
        # Use w (!!) not freq -.-
        F_ground =  np.array(list(map(lambda omega: compute_F_str_Wait(omega,distance_m, h, sigma1, sigma2, eps_r1, eps_r2), w)))
        F_ground_upper = np.conj(F_ground[::-1])
        #freq_minus = -freq[::-1]
        F_ground = np.hstack([F_ground[0:-1], F_ground_upper])
        freq_full = np.hstack([freq[0:-1], freq[-1]+freq])

        #f_ground = np.roll(ifft(F_ground), 1) 
        f_ground = ifft(F_ground)
        t_arr = np.array([i*dt for i in range(len(f_ground))])*1e6

        fields["efield_filtered"] = np.convolve(overlay["efield"], f_ground, mode="full")
        fields["hfield_filtered"] = np.convolve(overlay["hfield"], f_ground, mode="full")
        ######################################################################################
        
        #F_ground =  np.array(list(map(lambda omega: compute_cooray_rubinstein(omega, sigma1, eps_r1), w)))
        F_ground =  np.array(list(map(lambda omega: compute_wave_tilt_formula(omega, sigma1, eps_r1), w)))

        
        t = overlay["time"]
        xsi=sigma1/(2*eps0*eps_r1)
        K_t = -np.sqrt(mu0/(eps_r1*eps0))*xsi * np.exp(-xsi*t)*(special.iv(1, xsi*t)-special.iv(0,xsi*t))*dt
        K_tot = K_t.copy()
        K_tot[0] = K_tot[0] - np.sqrt(mu0/(eps_r1*eps0))
        # plt.figure()
        # plt.plot(t*1e6,K_t, label="K(t) time domain")
        # plt.legend()
        plt.gca().set_xlabel("Time (μs)")
        

        F_ground_upper = np.conj(F_ground[::-1])
        #freq_minus = -freq[::-1]
        F_ground = np.hstack([F_ground[0:-1], F_ground_upper])

        freq_full = np.hstack([freq[0:-1], freq[-1]+freq])
        plt.figure()
        plt.plot(freq_full/1000, F_ground.real, label="real")
        plt.plot(freq_full/1000, F_ground.imag, label="imag")
        plt.legend()
        plt.gca().set_xlabel("Fq (kHz)")
        #f_ground = np.roll(ifft(F_ground), 1) 
        f_ground = ifft(F_ground)
        t_arr = np.array([i*dt for i in range(len(f_ground))])*1e6


        # plt.figure()

        # plt.plot(t_arr, f_ground.real, label = "IFFT")
        # plt.legend()
        
        # plt.figure()

        # plt.plot(t_arr, f_ground.real, label = "IFFT (inverse, -1)")
        # plt.plot(t*1e6,-K_t, label="K(t) time domain")
        # plt.legend()


#        fields["efield_filtered"] = np.convolve(fields["hfield_filtered"], K_t, mode="full")
        #fields2 = np.convolve(overlay["hfield"], -K_tot, mode="full")
        #fields3 = np.convolve(overlay["efield"], f_ground, mode="full")
        #fields3 = np.convolve(fields["hfield_filtered"], f_ground, mode="full")
        fields3 = np.convolve(fields["efield_filtered"], f_ground, mode="full")
        time_vector = (np.array(overlay["time"]))*1e6
        
        #F_ground = np.array(list(map(lambda omega: compute_underground_response_w(omega, sigma1, eps_r1, underground_m), w)))
        F_ground = np.array(list(map(lambda omega: compute_rubinstein_1996_eq3(omega, sigma1, eps_r1, underground_m), w)))

        F_ground_upper = np.conj(F_ground[::-1])
        #freq_minus = -freq[::-1]
        F_ground = np.hstack([F_ground[0:-1], F_ground_upper])
        freq_full = np.hstack([freq[0:-1], freq[-1]+freq])
        plt.figure()
        plt.plot(freq_full/1000, F_ground.real, label="real")
        plt.plot(freq_full/1000, F_ground.imag, label="imag")
        plt.legend()
        #f_ground = np.roll(ifft(F_ground), 1) 
        f_ground = ifft(F_ground)
        t_arr = np.array([i*dt for i in range(len(f_ground))])*1e6
        plt.figure()

        plt.plot(t_arr, f_ground.real, label = "IFFT(underground formulation)")
        plt.legend()
        
        fields_underground = np.convolve(fields3, f_ground, mode="full")
        plt.figure()
        plt.plot(fields_underground[0:len(t_arr)], label = "Field underground")
        plt.legend()
        
        print("time vec len", len(time_vector))
        print("overlay hfield len", len(overlay["hfield"]))
        
        if plot_filtered:
            
            
            field = "efield_filtered" if plot_field == "efield" else "hfield_filtered"
#            ax.plot(time_vector + time_offset_us, fields[field][0:len(time_vector)], color=field_color, linestyle=filtered_linestyle, linewidth=linewidth, label= "H*K_t (no sqrt(μ0/ε)*δ(t-τ))")
            #ax.plot(time_vector + time_offset_us, fields3[0:len(time_vector)], color="black", linestyle="-", linewidth=linewidth, label= "Ez conv ifft(F_ground)")
            #ax.plot(time_vector + time_offset_us, fields3[0:len(time_vector)], color="black", linestyle="-", linewidth=linewidth, label= "Hφ conv with IFFT{CR(jω)}")
            ax.plot(time_vector + time_offset_us, fields3[0:len(time_vector)], color="black", linestyle="-", linewidth=linewidth, label= "Ex (from Ez through Wave tilt formulation")            
            ax.plot(time_vector + time_offset_us, fields_underground[0:len(time_vector)], color="blue", linestyle="-", linewidth=linewidth, label= "Referene Ex (z = {}m)".format(underground_m))

            #ax.plot(time_vector + time_offset_us, fields2[0:len(time_vector)], color="black", linestyle="--", linewidth=linewidth, label= "H*K_tot (with δ(t-τ))")
            #ax.plot(time_vector[0: len(overlay["hfield"])]+ time_offset_us, np.sqrt(mu0/(eps_r1*eps0))*overlay["hfield"], label = "mu0/eps * H")
            #ax.plot(time_vector[0: len(overlay["hfield"])] + time_offset_us, -fields[field][0: len(overlay["hfield"])]*dt+np.sqrt(mu0/(eps_r1*eps0))*overlay["hfield"], linestyle="-.", label = "Subtraction of H*K_t - mu0/eps * H")
            plt.legend()
            #ax.set_ylim(0, 1.1*np.max(fields[field][0:len(time_vector)]))
    
            

if __name__ == "__main__":
    
    from Wait_stratified_ground import *
    from utils import *
    
    fig = plt.figure(figsize=(6,5))
    ax1 = plt.gca()
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    ax1.set_zorder(ax2.get_zorder()+1)
    ax1.patch.set_visible(False)
    
    distance_km = 10
    file_path = "../theoretical/PEC_{}km".format(distance_km)
    kwargs = {"distance_km": distance_km,
              "h": 20, # height of the topmost layer as described in Shoory et al. 2010
              "sigma1": 1e-3,
              "sigma2": 40,
              "eps_r1": 10,
              "eps_r2": 1, # irrelevant for the considered scenarios
              "plot_PEC": False,
              "plot_filtered": True,
              "time_snippet_us": 20,
              "plot_field": "hfield",
              "field_color": "blue",
              "file_path": file_path,
              "ax": ax1,}
    
    plot_reference_fields(**kwargs)
    ax1.legend(loc=1)    
    ax2.legend(loc=4)