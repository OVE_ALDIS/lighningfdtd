# -*- coding: utf-8 -*-
"""
Created on Mon Aug 29 10:32:11 2022

@author: kohlmann
"""

import matplotlib.pyplot as plt
from scipy import signal, special
from scipy.fftpack import fft,ifft
#from scipy import special
import numpy as np

c0 = 299792458
mu0 = 4*np.pi*1e-7
eps0 = 1/(c0**2 * mu0)

def compute_F_str_Wait(w, dist, h1, sigma_1, sigma_2, eps_r1, eps_r2):
    """
    Computes the Wait's formulation of stratified ground
    as given in Shoory 2010
    
    Input: w ... angular frequency omega
    """
    
    gamma0 = 1j*w*np.sqrt(mu0 * eps0)
    gamma1 = np.sqrt(1j*w*mu0*(sigma_1+1j*w*eps0*eps_r1))    
    gamma2 = np.sqrt(1j*w*mu0*(sigma_2+1j*w*eps0*eps_r2))    
    u1 = np.sqrt(gamma1**2 - gamma0**2)
    u2 = np.sqrt(gamma2**2 - gamma0**2)
    K1 = u1/(sigma_1 + 1j*w*eps0*eps_r1)
    K2 = u2/(sigma_2 + 1j*w*eps0*eps_r2)

    delta_str = np.sqrt(eps0/mu0)*K1*(K2+K1*np.tanh(u1*h1))/(K1+K2*np.tanh(u1*h1))
    p_str = -0.5*gamma0*dist*delta_str**2
    F_str = 1 - 1j*np.sqrt(np.pi*p_str)*np.exp(-p_str)*special.erfc(1j*np.sqrt(p_str))

     # at frequency 0, it evaluates "to not a number", also for frequencies above about 7.5 MHz
    if h1<=20 and w/(2*np.pi) > 10e6:
        F_str = 0+0*1j
    if np.isnan(np.abs(F_str)) and np.isclose(w,0): # at frequency 0, it evaluates "to not a number", also for frequencies above about 7.5 MHz
        return 1+0*1j
    if np.isnan(np.abs(F_str)) and w>0:
        return 0+0*1j
    if np.abs(F_str) > 1e6:
        return 0+0*1j
    return F_str

def compute_F_str_Wait_test(w, dist, h1, sigma_1, sigma_2, eps_r1, eps_r2):
    """
    Computes the Wait's formulation of stratified ground
    as given in Shoory 2010
    
    Input: w ... angular frequency omega
    """
    
    gamma0 = 1j*w*np.sqrt(mu0 * eps0)
    print("γ_0", gamma0)
    gamma1 = np.sqrt(1j*w*mu0*(sigma_1+1j*w*eps0*eps_r1))    
    print("γ_1", gamma1)
    gamma2 = np.sqrt(1j*w*mu0*(sigma_2+1j*w*eps0*eps_r2))    
    print("γ_2", gamma2)
    u1 = np.sqrt(gamma1**2 - gamma0**2)
    print("u_1", u1)
    u2 = np.sqrt(gamma2**2 - gamma0**2)
    print("u_2", u2)
    K1 = u1/(sigma_1 + 1j*w*eps0*eps_r1)
    print("K1", K1)
    K2 = u2/(sigma_2 + 1j*w*eps0*eps_r2)
    print("K2", K2)

    delta_str = np.sqrt(eps0/mu0)*K1*(K2+K1*np.tanh(u1*h1))/(K1+K2*np.tanh(u1*h1))
    print("Δ_str", delta_str)
    p_str = -0.5*gamma0*dist*delta_str**2
    print("p_str", p_str)
    F_str = 1 - 1j*np.sqrt(np.pi*p_str)*np.exp(-p_str)*special.erfc(1j*np.sqrt(p_str))
    print("F_str", F_str)
    if np.isnan(F_str) and np.isclose(w,0): # at frequency 0, it evaluates "to not a number", also for frequencies above about 7.5 MHz
        return 1+0*1j
    elif np.isnan(F_str) and w>0:
        return 0+0*1j
    else:
        return F_str
def norton_filter_WRONG(w, distance, sigma, eps_r):
    """
    This filter has inverse angle behaviour ...
    """
    fq = w/(2*np.pi)
    lam = c0/fq
    k = 2*np.pi/lam
    x = (1/(2*np.pi*eps0))*sigma/fq
    u2 = 1.0 / (eps_r+x*1j)
    u = np.sqrt(u2)
    w_num=(1j*k*distance*u2*(1-u2))/2
    z1 = -1j*np.sqrt(w_num)
    
    Ferfc = special.erfc(z1)
    filt = 1+1j*np.sqrt(np.pi*w_num)*np.exp(-w_num)*Ferfc
    
    return filt

def norton_filter(w, distance, sigma, eps_r):
    k0 = w*np.sqrt(eps0*mu0)
    k1 = np.sqrt(-1j*w*mu0*(sigma+1j*eps0*eps_r))
    delta1 = k0/k1*np.sqrt(1-k0**2/k1**2)
    eta = -1j*w*distance/(2*c0)*delta1**2
        
    filt = 1-1j*np.sqrt(np.pi*eta)*np.exp(-eta)*special.erfc(1j*np.sqrt(eta))
    
    if np.isnan(filt):
        return 0+0*1j
    else:
        return filt

def wait_time_filter(t, dist, sigma, epsilon, simple=False):
    """
    simple = True neglects the last term in 11.37 (Cooray, The Lightning Flash) -> no displacement current (epsilon disregarded)
    """
    xsi_sq = dist/(2*mu0*sigma*c0**3)
    beta = 1/(mu0*sigma*c0**2)
    dt = t[-1]-t[-2]
    if simple:
        
        derivative = 1/dt*np.diff(1-np.exp(-t**2/(4*xsi_sq)))
        #test = np.exp(-t**2/(4*xsi_sq))*t/(2*xsi_sq)

    else:
        x = t/(2*np.sqrt(xsi_sq))
        Q = x**2*(1-x**2)*np.exp(-x**2)
        #derivative = 1/dt*np.diff(1-np.exp(-t**2/(4*xsi_sq))+2*beta*(epsilon+1)*Q/t)
        derivative = 1/dt*np.diff(1-np.exp(-t**2/(4*xsi_sq))+beta*np.sqrt(sigma/1e-4)*(epsilon+1)/epsilon*Q/t)
    return derivative

    

if __name__ == "__main__":
    del_f = 1000
    f_full = np.r_[-10e6:10e6+del_f:del_f]
    del_t = 1/(len(f_full)*del_f)
    w_full = 2*np.pi*f_full
    f = np.r_[10:3.4e6+1:10]
    f_= np.r_[-3.4e6:10:-10]
    w = f*2*np.pi
    w_ = f_*2*np.pi
    dist = 100000
    sigma1 = 1e-3
    sigma2 = 4
    eps_r1 = 10
    eps_r2 = 30
    
    # F_str_1000m_full = np.array(list(map(lambda omega: compute_F_str_Wait(omega,dist,1000, sigma1, sigma2, eps_r1, eps_r2), w_full)))
    
    # F_str_1000m = np.array(list(map(lambda omega: compute_F_str_Wait(omega,dist,1000, sigma1, sigma2, eps_r1, eps_r2), w)))
    # fig, (ax1, ax2) = plt.subplots(2, 1)
    # ax1.plot(f*1e-6, np.abs(F_str_1000m), "-.r", label="h1 = 1000 m")
    # ax2.plot(f*1e-6, np.angle(F_str_1000m)*180/np.pi, "-.r", label="h1 = 1000 m")
    # plt.figure()
    # plt.plot(f*1e-6, np.real(F_str_1000m), "-r", label="Real")
    # plt.plot(f*1e-6, np.imag(F_str_1000m), "-b", label="Imag")
    # plt.legend()
    # plt.grid()
    
    # F_str_1000m_minus =  np.array(list(map(lambda omega: compute_F_str_Wait(omega,dist,1000, sigma1, sigma2, eps_r1, eps_r2), w_)))
    
    # F_str = np.hstack([F_str_1000m_minus,F_str_1000m])
    # w = np.hstack([w_,w])
    # f_str = ifft(F_str)
    
    # f_str_full = ifft(F_str_1000m_full)
    
    # plt.figure()
    # f_str = np.roll(f_str_full, int(len(f_str_full)/2))
    # plt.plot(np.abs(f_str), label = "Absolute")
    # plt.plot(np.real(f_str), label= "real")
    # plt.plot(np.imag(f_str), label = "imag")
    # plt.gca().legend()
    # plt.gca().grid()
    
    
    f = np.logspace(3, 7, num=10000)
    w = 2*np.pi*f
    
    F_str_2m = list(map(lambda omega: compute_F_str_Wait(omega,dist,2, sigma1, sigma2, eps_r1, eps_r2), w))
    F_str_20m = list(map(lambda omega: compute_F_str_Wait(omega,dist,20, sigma1, sigma2, eps_r1, eps_r2), w))
    F_str_0m = list(map(lambda omega: compute_F_str_Wait(omega,dist,0, sigma1, sigma2, eps_r1, eps_r2), w))
    F_str_1000m = list(map(lambda omega: compute_F_str_Wait(omega,dist,1000, sigma1, sigma2, eps_r1, eps_r2), w))

    F_norton_1000m =  list(map(lambda omega: norton_filter(omega,dist,sigma1, eps_r1), w))
    
    
    fig, (ax1, ax2) = plt.subplots(2, 1)
    ax1.semilogx(f*1e-6, np.abs(F_str_2m), "--b", label="h1 = 2 m")
    ax1.semilogx(f*1e-6, np.abs(F_str_20m),"--g", label="h1 = 20 m")
    ax1.semilogx(f*1e-6, np.abs(F_str_0m), "-k", label="h1 = 0 m")
    ax1.semilogx(f*1e-6, np.abs(F_str_1000m), "-.r", label="h1 = 1000 m")
    ax1.semilogx(f*1e-6, np.abs(F_norton_1000m), "--c", label="Norton approx")
    
    ax1.set_ylabel("Magnitude")
    ax1.grid()
    
    ax2.semilogx(f*1e-6, np.angle(F_str_2m)*180/np.pi, "--b", label="h1 = 2 m")
    ax2.semilogx(f*1e-6, np.angle(F_str_20m)*180/np.pi, "--g", label="h1 = 20 m")
    ax2.semilogx(f*1e-6, np.angle(F_str_0m)*180/np.pi, "-k", label="h1 = 0 m")
    ax2.semilogx(f*1e-6, np.angle(F_str_1000m)*180/np.pi, "-.r", label="h1 = 1000 m")
    ax2.semilogx(f*1e-6, np.angle(F_norton_1000m)*180/np.pi, "--c", label="Norton approx")
    
    ax2.set_ylabel("Phase (°)")
    ax2.set_xlabel("f (MHz)")
    ax2.grid()
    fig.suptitle("Magnitude and phase of strat. ground attenuation\nDistance: {}km".format(dist/1000))
    

    fig.tight_layout()
    
    t = np.r_[0:100e-6:0.1e-6]
    wait_time_filter(t, dist, sigma1, eps_r1, simple=True)
    
    
    F_str_1000m = F_str_1000m - 2j*np.imag(F_str_1000m)
 #   ax1.semilogx(f*1e-6, np.abs(F_str_1000m), "-.r", label="h1 = 1000 m inverted Im for f>0")
#    ax2.semilogx(f*1e-6, np.angle(F_str_1000m)*180/np.pi, "-.r", label="h1 = 1000 m inverted Im for f>0")
    
    ax1.legend()
    ax2.legend()
    
    # Real and imaginary plot
    h = 1000   
    dist = 100000
    F_str_1000m_full = np.array(list(map(lambda omega: compute_F_str_Wait(omega,dist,h, sigma1, sigma2, eps_r1, eps_r2), w_full)))
    plt.figure()
    plt.title("Frequency domain filter function (ρ={} km, h={} m)".format(int(dist/1000),h))
    plt.plot(f_full/1e6,np.abs(F_str_1000m_full), label = "absolute")
    plt.plot(f_full/1e6, np.real(F_str_1000m_full), label= "real")
    plt.plot(f_full/1e6,np.imag(F_str_1000m_full), label = "imag")
    plt.gca().set_xlabel("f (MHz)")
    plt.gca().set_ylabel("H(jf)")
    plt.gca().set_xlim(-4,4)
    plt.gca().set_ylim(-4,4)
    plt.gca().legend()
    plt.gca().grid()

    # plt.figure()

    # plt.title("Time domain filter function (Inverse FFT)")
    # f_str = np.roll(f_str_full, int(len(f_str_full)/2))
    # plt.plot(np.abs(f_str), label = "Absolute")
    # plt.plot(np.real(f_str), label= "real")
    # plt.plot(np.imag(f_str), label = "imag")
    # plt.gca().legend()
    # plt.gca().grid()