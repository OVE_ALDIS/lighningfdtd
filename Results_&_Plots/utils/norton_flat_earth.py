"""
Created on Tue May 14 15:21:09 2019

@author: hannes
"""
import matplotlib.pyplot as plt
from scipy import signal, special
from scipy.fftpack import fft,ifft
#from scipy import special
import math as m
import numpy as np

c0 = 2.998e8
mu0 = 4*np.pi*10**(-7)
eps0 = 8.8541878128e-12

class BesselFilter:
    def __init__(self, order, fq, typ="Bessel"):
        self.order = order
        self.upper_fq = fq
        self.typ = typ
        if typ=="Bessel":
            self.b,self.a = signal.bessel(self.order, self.upper_fq, btype='low', analog=True, output='ba')
        elif typ == "Butterworth":
            self.b,self.a = signal.butter(self.order, self.upper_fq, btype='low', analog=True, output='ba')
            
    def filter_signal(self, input_signal, dt, typ="Bessel"):
        nyquist_fq = 1/dt/2
        if self.upper_fq > nyquist_fq:
            return input_signal
        else:
            if typ=="Bessel":
                self.b,self.a = signal.bessel(self.order, self.upper_fq/nyquist_fq, btype='low', analog=False, output='ba')
                output_signal = signal.filtfilt(self.b, self.a, input_signal)
            elif typ == "Butterworth":
                    self.b,self.a = signal.butter(self.order, self.upper_fq/nyquist_fq, btype='low', analog=False, output='ba')
                    output_signal = signal.filtfilt(self.b, self.a, input_signal)
        return output_signal
    
    def get_freqs(self):
        return signal.freqs(self.b, self.a)
    def get_upper_fq(self):
        return self.upper_fq

typ = "Bessel"
order = 2
filter_fq = 350
filt = BesselFilter(order, filter_fq*1e3, typ = typ)
w, h = filt.get_freqs()
typ2 = "Butterworth"
order2 = 2
filter_fq2 = 350
filt2 = BesselFilter(order2, filter_fq2*1e3, typ=typ2)
w2, h2 = filt2.get_freqs()
typ3 = "Butterworth"
order3 = 8
filter_fq3 = 350
filt3 = BesselFilter(order3, filter_fq3*1e3, typ=typ3)
w3, h3 = filt3.get_freqs()

def norton_filter(sigma, eps_r, distance, samples=1e6, typ = 'lin', from_to = [0,1e6]):


    f = None
    if typ == 'lin': 
        fq = np.linspace(from_to[0], from_to[1], samples)
    else:
        fq = np.logspace(from_to[0], np.log10(from_to[1]), samples+1, base=10)     
    lam = c0/fq
    k = 2*np.pi/lam
    x = (1/(2*np.pi*eps0))*sigma/fq
    u2 = 1.0 / (eps_r+x*1j)
    u = np.sqrt(u2)
    w_num=(1j*k*distance*u2*(1-u2))/2
    z1 = -1j*np.sqrt(w_num)
    
    Ferfc = special.erfc(z1)
    filt = 1+1j*np.sqrt(np.pi*w_num)*np.exp(-w_num)*Ferfc
    #filt.real = 2 - filt.real
    
    imp = ifft(filt)
    return fq, filt, imp

def wait_formulation(sigma, eps_r, distance, samples=1e6, typ='lin', from_to = [0,1e6]):
    """
    Taken from Cooray, "The Lightning Flash" Chapter 11.5 (Simplified procedures to calculate electric 
                                                           and magnetic fields over finitely conducting ground)

    Parameters
    ----------
    sigma : TYPE
        DESCRIPTION.
    eps_r : TYPE
        DESCRIPTION.
    distance : TYPE
        DESCRIPTION.
    samples : TYPE, optional
        DESCRIPTION. The default is 1e6.
    typ : TYPE, optional
        DESCRIPTION. The default is 'lin'.
    from_to : TYPE, optional
        DESCRIPTION. The default is [0,1e6].

    Returns
    -------
    fq : TYPE
        DESCRIPTION.
    w1_1 : TYPE
        DESCRIPTION.
    imp : TYPE
        DESCRIPTION.

    """
    if typ == 'lin': 
        fq = np.r_[from_to[0]:from_to[1]:1/5e-8]
    else:
        fq = np.logspace(from_to[0], np.log10(from_to[1]), samples+1, base=10)   
        
    omega = 2*np.pi*fq
    k0_sq = (omega)**2*mu0*eps0
    k1_sq = -1j*omega*mu0*(sigma + 1j*omega*eps0*eps_r)
    delta1 = np.sqrt(k0_sq/k1_sq)*np.sqrt(1-(k0_sq/k1_sq))
    eta = -1j*omega*distance/(2*c0)*delta1**2
    w1_1 = 1 - 1j*np.sqrt(np.pi*eta)*np.exp(-eta)*special.erfc(1j*np.sqrt(eta))
    
    imp = ifft(w1_1)
    print("TEST {}".format(sigma))
    print(imp)
    print("END TEST")
    return fq, w1_1, imp

if __name__=="__main__":
    eps_r = 10
    sig = [0.001, 0.005, 0.01]
    distances = [100000]
    root_idx = 0
    for idx, distance in enumerate(distances):
        for sigma in sig:
            fq, filt, imp = norton_filter(sigma, eps_r, distance, samples=2**20, typ="lin", from_to=[1,1e8])
            # fq, filt, imp = wait_formulation(sigma, eps_r, distance, samples=2**20, typ="lin", from_to=[1,1e8])

            # plt.figure(root_idx+0+idx*3)
            # plt.plot(fq, np.real(filt), label="real")
            # plt.plot(fq, np.imag(filt), label="imag")
            # plt.plot(fq,np.abs(filt), label="abs")
            # plt.legend()
            # plt.show()
            # plt.figure(root_idx+1+idx*3)
            # plt.plot(imp, label="h[n]")
            # plt.legend()
            # plt.show()
            
            plt.figure(root_idx+2+idx*3)
            plt.semilogx(fq/1e3, 20*np.log10(np.abs(filt)), label="Norton filter, σ = "+str(sigma*1000)+" mS/m")
            plt.gca().set_ylabel("Attenuation [dB]")
            plt.gca().set_xlabel("f [kHz]")
        
        plt.figure(root_idx+2+idx*3)
        #plt.semilogx(w/1e3, 20*np.log10(np.abs(h)), "k--", label=typ+", order "+str(order) +" ("+str(filter_fq)+" kHz)")
        plt.semilogx(w/1e3, 20*np.log10(np.abs(h2)), "k-.", label=typ2+", order "+str(order2) +" ("+str(filter_fq2)+" kHz)")
        plt.semilogx(w/1e3, 20*np.log10(np.abs(h3)), "k:", label=typ3+", order "+str(order3) +" ("+str(filter_fq3)+" kHz)")
        plt.gca().set_xlim(50, 1e4)
        plt.gca().set_ylim(-60, 5)
        plt.grid()
        plt.legend()
        plt.title("Distance: "+str(distance/1e3)+"km")
        
    
