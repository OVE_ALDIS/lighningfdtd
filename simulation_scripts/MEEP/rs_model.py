# -*- coding: utf-8 -*-
"""
Created on Tue Mar 14 16:37:25 2023

@author: kohlmann
"""

import numpy as np
import scipy
from scipy import interpolate

import csv
import math as m
import matplotlib.pyplot as plt

c0 = 299792458

class RS_Model:
    def __init__(self, src_configs, t_ord_of_magnitude=1):
        """
        i_params is a list of dicts, which all hold Heidler function parameters: 
        {"i":1, "tau1":1.8, "tau2":95,"n":2,"t_ord_of_magn":"mus"}

        
        src_configs is structured this way:
        src_configs = {"src_x": src_x,
           "surface_z": surface_z,
           "v_rs": v_rs,
           }
        """
        self.i_params = src_configs["channel_base_current"]

        self.current_type = "analytical"
        self.filename = None

        self.t_ord_of_magnitude = t_ord_of_magnitude
        self.src_configs = src_configs
        self.mtlm = self.src_configs["mtlm"]
        self.mtlm["lambda"] = self.mtlm["lambda"]
        
        self.mp_time_conversion_factor
        
        
    def mtlm_factor(self, height):
        """
        height must come in MEEP units
        """
        if self.mtlm == {}:
            return 1
        
        if self.mtlm["lambda"]==0:
            return 1
        
        if self.mtlm["type"] == "exp":
                return m.exp(-height/self.mtlm["lambda"])
        elif self.mtlm["type"] == "lin":
            fact = (self.mtlm["lambda"]-height)/self.mtlm["lambda"]
            if fact>0:
                return fact
            else:
                return 0
        
    def _current_fct(self, height, t_shift, mp_t_conversion_factor, t_shift_heaviside=0):
        """
        t_shift_heaviside is an extra time retardation function to check (t<t_shift_heaviside) when the source may activate (e.g. if two step functions corcur)

        """
        def wrapper(t):
            eta = []
            t_i = [] # this is just a variable substitution used for the function
            current_function = 0
            t_orig = t
            t = t_orig-t_shift
           
            # return values right away before even checking which type of function, if time certain criteria are satisfied
            if t<0:  return 0
            if t<t_shift_heaviside-t_shift:  return 0
            
            # Set mtlm_factor (can also be checked in advance, since it is applicable to every model!)

            mtlm_factor = self.mtlm_factor(height)
            

            for idx, i_params in enumerate(self.i_params):
                eta.append(m.exp(-i_params["tau1"]/i_params["tau2"]*(i_params["n"] \
                                 *i_params["tau2"]/i_params["tau1"])**(1/i_params["n"])))
                t_i.append((t/i_params["tau1"])**i_params["n"])
                current_function += i_params["i"]/eta[idx]*t_i[idx]/(t_i[idx]+1) * m.exp(-t/i_params["tau2"])
            
            return mtlm_factor*current_function

        return wrapper
    
    def stroke(self):
        print("Subs. stroke (SU)\n: " + str(self.i_params))
        

    def sources(self):
        """
        This function builds the source array that the MEEP simulation can readily use in the Sim(...) instanciation
        and can be called like RS_Model(...).sources()
        
        src_configs is structured this way:
        src_configs = {"src_x": src_x,
               "src_z": src_z,
               "v_rs": v_rs,
               "sources_per_unit": sources_per_unit, 
               "stroke_length": stroke_length,
               "source_component":mp.Ez,
               }
        
        This function does also all the work to include the tower model source currents
        """
        sc = self.src_configs
        sources = []
        v_rs = sc["v_rs"]

        sources_per_unit = sc["sources_per_unit"]
        source_extent = 1/sources_per_unit
        src_offset=0#source_extent/2#source_extent/2 # shifts the full source array up by source_extent/2
        stroke_length = sc["stroke_length"]
        source_component = sc["source_component"]
        
        src_x = sc["src_x"]
        surface_z = sc["surface_z"]+src_offset
        
        src_z = surface_z
            
        total_nr_src = int(stroke_length*sources_per_unit)
        if point_src: # in case that just a point source shall be active for testing
            total_nr_src = 1
        total_nr_src_tower = int(h_tower*sources_per_unit) # height error of max source_extent, if truncated
        # ************************************************************************
        # *************      RS ENGINEERING MODEL, no tower    *******************
        # ************************************************************************
        for i in range(total_nr_src):
            h = i*source_extent # z in m --> i/nr_src_per_unit*1000m
            start_time = h/(v_rs/c0)

            factor = 1
            sources.append(mp.Source(mp.CustomSource(self._current_fct(height=h, 
                                                                      t_shift=start_time, 
                                                                      channel=True,
                                                                      wall_src=wall_src),
                                                     start_time = start_time, # before it is 0!
                                                     end_time = 100),
                                    amplitude = factor, #amplitude scaling
                                    component=source_component,
                                    center=mp.Vector3(src_x,0,src_z+h), 
                                    size=mp.Vector3(0,0,source_extent)))

        return sources
        # ************************************************************************